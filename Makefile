all:
	cp src/lexer.py bin/lexer.py
	cp src/parser.py bin/parser.py
	python -m py_compile bin/lexer.py
	mv bin/lexer.pyc bin/lexer
	chmod +x bin/lexer
	python -m py_compile bin/parser.py
	mv bin/parser.pyc bin/parser
	chmod +x bin/parser
clean:
	rm -rf bin/*


