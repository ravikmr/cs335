import pprint

class symbolTable:
    # Constructor for the function
    def __init__(self):
        self.symbolTable = {
                'main': {
                    '__scopeName__': 'main', 
                    '__parentName__': 'main', 
                    '__type__':'FUNCTION', 
                    '__returnType__': 'UNDEFINED',
                    '__level__' : 0
                    }
                }

        self.offset = [0]
        self.scope = [self.symbolTable['main']]

        self.tempBase = "t"
        self.tempCount = 0

    def printSymbolTable(self):
    pprint.pprint(self.symbolTable)

    def lookup(self, identifier):
        # Obtain the currentScope
        scopeLocation = len(self.scope)
        return self.lookupScope(identifier, scopeLocation - 1)

    def lookupScope(self, identifier, scopeLocation):
        if scopeLocation == -1:
            return None