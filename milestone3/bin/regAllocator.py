class regAllocator:

	def __init__(self):
		self.varDescripter = {}
		self.regDescriptor = {}
		self.usedReg = []
		self.asmInstr = ""
		self.freeReg = ["$s0","$s1","$s2","$s3","$s4","$s5","$s6","$s7","$t0","$t1","$t2","$t3","$t4","$t5","$t6","$t7"]

	def getRegister(self,arg,var,varToFree):
		#check if array type
		isArray = False
		if '[' in arg and ']' in arg:
			isArray = True
			ind1 = arg.index('[')
			ind2 = arg.index(']')
			arrayIndex = arg[ind1+1:ind2]
			arg = arg[:ind1]
			var=arg
		for key, value in self.regDescriptor.iteritems():	#find if value is already present in some register
			if value == arg:
				self.asmInstr = ""
				if isArray:
					indInt = int(arrayIndex)
					indInt = 4*indInt
					key = str(indInt)+"("+key+")"
				return (key,self.asmInstr)

		if len(self.freeReg) > 0:	#if there is a register available
			reg = self.freeReg.pop(0)	#pop a free register
			self.usedReg.append(reg)	#append that register to list of used registers
			self.regDescriptor[reg] = arg	#this register now contains value of arg
			self.varDescripter[var] = reg   #this variable is now contained in reg 
			#outputMain.append("lw "+reg+","+var)
			if isArray:
				indInt = int(arrayIndex)
				indInt = 4*indInt
				self.asmInstr = "la "+reg+","+var
				#for array, return index(reg)
				reg = str(indInt)+"("+reg+")"
				
			else:
				self.asmInstr = "lw "+reg+","+var
			return (reg,self.asmInstr)
		elif len(self.freeReg) == 0:	#if no register is available
			if varToFree != "":
				reg = ""
				for key,value in self.regDescriptor.iteritems():
					if value == varToFree:
						reg = key
						self.regDescriptor[reg] = arg
						for key1,value1 in self.varDescripter.iteritems():	#find the variable for which this register was being used. nullify it
							if value1 == reg:
								self.varDescripter[key1] = ""
						self.varDescripter[var] = reg
						if isArray:
							indInt = int(arrayIndex)
							indInt = 4*indInt
							self.asmInstr = "la "+reg+","+var
							reg = str(indInt)+"("+reg+")"
						else:
							self.asmInstr = "lw "+reg+","+var
				return (reg,self.asmInstr)
			else:
				reg = self.usedReg.pop(0)
				self.regDescriptor[reg] = arg
				for key,value in self.varDescripter.iteritems():	#find the variable for which this register was being used. nullify it
					if value == reg:
						self.varDescripter[key] = ""
				self.varDescripter[var] = reg      #set this variable to be contained in register
				#outputMain.append("lw "+reg+","+var)
				if isArray:
					indInt = int(arrayIndex)
					indInt = 4*indInt
					self.asmInstr = "la "+reg+","+var
					reg = str(indInt)+"("+reg+")"
				else:
					self.asmInstr = "lw "+reg+","+var
					self.usedReg.append(reg)
				
				return (reg,self.asmInstr)