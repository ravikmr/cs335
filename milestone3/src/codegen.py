#MIPS-> http://logos.cs.uic.edu/366/notes/mips%20quick%20tutorial.htm

#problems: (solved) after allocating register, not loading value into register from memory, directly using.
# 			register allocation is not next use heuristics
# (solved)if else me else ka qa hoga
# (solved)a = g krna hai, reg1 = reg2, but a=g kahan hua ? so register descriptor update krna hoga
import sys
import regAllocator

regAllocObj = regAllocator.regAllocator()

debug = 0


def nextUseHueristics(currIndex,regList):
	#find which variable is used farthest
	regDesc = regList
	varToFree = ""
	index = -1
	#currIndex = tacString.find(line)
	#print "currIndex ",currIndex

	for key,value in regDesc.iteritems():
		#print key,value
		if tacString[currIndex:].find(','+value+',') > index:
			index = tacString[currIndex:].find(','+value+',')
			varToFree = value
	#print "key hai ",varToFree
	return varToFree

def convertToAsm(line,lineNum):
	global exitFound
	asmInstruc = ""
	word = line.split(',')

	if exitFound == False and word[0] != "init_val":
		outputMain.append("Label_"+str(lineNum)+":")
	

	if word[0] == "#":
		#comment
		pass

	elif word[0] == "label":
		outputMain.append("Label_"+word[1]+":")

	elif word[0] == "ret_label":
		pass

	elif word[0] == "funcLabel":
		outputMain.append(word[1]+":")

	elif word[0] == "callFunc":
		#store all reg in stack, for nested functions
		#store ra of all functions, then recover back
		regDesc = regAllocObj.regDescriptor
		for key,value in regDesc.iteritems():
			outputMain.append("addi $sp,$sp,-4")
			outputMain.append("sw "+key+",($sp)")
		outputMain.append("addi $sp,$sp,-4")
		outputMain.append("sw $ra,($sp)")
		outputMain.append("jal "+word[1])

		outputMain.append("lw $ra,($sp)")
		outputMain.append("addi $sp,$sp,4")
		for key,value in regDesc.iteritems():
			outputMain.append("lw "+key+",($sp)")
			outputMain.append("addi $sp,$sp,4")

	elif word[0] == "return_main":
		if word[1] == "1":
			outputMain.append("li $v0,1")
		else:
			outputMain.append("lw $v0,"+word[1])

	elif word[0] == "ret":
		#print "yahan/////////////////, ",word[1]
		if word[1] == "1":
			outputMain.append("li $v0,1")
		else:
			outputMain.append("lw $v0,"+word[1])
		outputMain.append("jr $ra")

	elif word[0] == "init_val":
		if word[1] != "array":
			outputData.append(word[1]+": .word "+word[2])
			initList.append(word[1])
		elif word[1] == "array":
			arrayLength = word[3]
			arrayName = word[2]
			outputData.append(arrayName+": .space "+arrayLength)
			# appendString = arrayName+": .byte"
			# for i in range(int(arrayLength)):
			# 	appendString = appendString + " '" +word[4+i]+"'"
			# outputData.append(appendString)



	elif word[0] == "exit":
		exitFound = True
		if debug:
			print "exit entered"
		outputMain.append("li $v0,10")
		outputMain.append("syscall")

	elif word[0] == "cond_goto":
		if debug:
			print "cond_goto entered"


		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		if word[1] == "==":
			if debug:
				print "cond_goto == entered"
			if not word[2].isdigit() and not word[3].isdigit():
				

				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("beq "+reg1+","+reg2+",Label_"+word[4])

		elif word[1] == ">=":
			if debug:
				print "cond_goto >= entered"
			if not word[2].isdigit() and not word[3].isdigit():
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bge "+reg1+","+reg2+","+word[4])

		elif word[1] == "<=":
			if debug:
				print "cond_goto <= entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("ble "+reg1+","+reg2+",Label_"+word[4])

		elif word[1] == "<":
			if debug:
				print "cond_goto < entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("blt "+reg1+","+reg2+",Label_"+word[4])

		elif word[1] == ">":
			if debug:
				print "cond_goto > entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bgt "+reg1+","+reg2+",Label_"+word[4])
		
		elif word[1] == "!=":
			if debug:
				print "cond_goto != entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bne "+reg1+","+reg2+",Label_"+word[4])
		

	elif word[0] == "|":
		if debug:
			print "| entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		outputMain.append("or "+reg1+","+reg2+","+reg3)

	elif word[0] == "+=":
		if debug:
			print "+= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("add "+reg1+","+reg1+","+reg2)

	elif word[0] == "-=":
		if debug:
			print "-= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sub "+reg1+","+reg1+","+reg2)	

	elif word[0] == "*=":
		if debug:
			print "*= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("mult "+reg1+","+reg1)
		outputMain.append("mflo "+reg1)

	elif word[0] == "/=":
		if debug:
			print "/= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("div "+reg1+","+reg2)
		outputMain.append("mflo "+reg1)

	elif word[0] == "%=":
		if debug:
			print "%= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		

		outputMain.append("div "+reg1+","+reg1)
		outputMain.append("mfhi "+reg1)

	elif word[0] == "^=":
		if debug:
			print "^= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		

		outputMain.append("xor "+reg1+","+reg1+","+reg2)

	elif word[0] == "^":
		if debug:
			print "^ entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("xor "+reg1+","+reg2+","+reg3)

	elif word[0] == "&":
		if debug:
			print "& entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("and "+reg1+","+reg2+","+reg3)

	elif word[0] == "==":
		if debug:
			print "== entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("seq "+reg1+","+reg2+","+reg3)

	elif word[0] == "!=":
		if debug:
			print "!= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sne "+reg1+","+reg2+","+reg3)

	elif word[0] == "<":
		if debug:
			print "< entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("slt "+reg1+","+reg2+","+reg3)

	elif word[0] == ">":
		if debug:
			print "> entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sgt "+reg1+","+reg2+","+reg3)

	elif word[0] == "<=":
		if debug:
			print "<= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sle "+reg1+","+reg2+","+reg3)

	elif word[0] == ">=":
		if debug:
			print ">= entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sge "+reg1+","+reg2+","+reg3)

	elif word[0] == "<<":
		if debug:
			print "<< entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("sllv "+reg1+","+reg2+","+reg3)

	elif word[0] == ">>":
		if debug:
			print ">> entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)

		outputMain.append("srlv "+reg1+","+reg2+","+reg3)

	elif word[0] == "=":
		if debug:
			print "= entered"

		# if word[1] == "array":
		# 	regDesc = regAllocObj.regDescriptor
		# 	currIndex = tacString.find(line)
		# 	varToFree = nextUseHueristics(currIndex,regDesc)
		# 	reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],"$"+varToFree)
		# 	reg2,asmInstruc = regAllocObj.getRegister(word[4],word[4],varToFree)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)
		#print "equal me ghusa"
		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")

		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		#print asmInstruc
		if asmInstruc != "":outputMain.append(asmInstruc)

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		#print "= me ",reg1,reg2
		#print asmInstruc
		# outputMain.append("lw "+reg2+","+word[2]) #$t1 = contant value
		# outputMain.append("lw "+reg1+","+word[1]) #$t1 = contant value
		if '(' in reg1:
			outputMain.append("sw "+reg2+","+reg1)
		elif ')' in reg2:
			outputMain.append("sw "+reg1+","+reg2)
		else:
			outputMain.append("move "+reg1+","+reg2) #variable = $t1
	elif word[0] == "+":
		#print "yoyo ",word[1],word[2],word[3]
		if debug:
			print "+ entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		if not word[2].isdigit() and not word[3].isdigit(): 
		# implementing add, addi is left
			reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)
			# outputMain.append("lw "+reg1+","+word[1]) #$t1 = contant value
			# outputMain.append("lw "+reg2+","+word[2]) #$t1 = contant value
			# outputMain.append("lw "+reg3+","+word[3]) #$t1 = contant value
			#outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
			#outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
			#print "ghusa",reg1,reg2,reg3
			outputMain.append("add "+reg1+","+reg2+","+reg3) # $t1 = $t1 + $t2
			#outputMain.append("sw "+reg1+","+word[1]) # variable 3 = $t1
		#else:
		#addi implementation here
	elif word[0] == "-":
		if debug:
			print "- entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		if not word[2].isdigit() and not word[3].isdigit(): 
		# implementing sub, subi is left
			reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)
			#outputMain.append("lw "+reg1+","+word[2]) # $t1 = variable 1
			#outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
			outputMain.append("sub "+reg1+","+reg2+","+reg3) # $t1 = $t1 - $t2
			#outputMain.append("sw $t1,"+word[1]) # variable 3 = $t1
		#else:
		#subi implementation here
	elif word[0] == "*":
		if debug:
			print "* entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")
		

		if not word[2].isdigit() and not word[3].isdigit(): 
		# implementing mult is left
			reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)
			#outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
			#outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
			outputMain.append("mult "+reg2+","+reg3) # $t1 = $t1 * $t2
			outputMain.append("mflo "+reg1) # $t1 = low
	elif word[0] == "/":
		if debug:
			print "/ entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")
		

		if not word[2].isdigit() and not word[3].isdigit(): 
			
			reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)
			#outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
			#outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
			outputMain.append("div "+reg2+","+reg3) # $t1 = $t1 / $t2
			outputMain.append("mflo "+reg1) # $t1 = low
	elif word[0] == "%":
		if debug:
			print "mod entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		if not word[2].isdigit() and not word[3].isdigit(): 
			
			reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg2,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)

			regDesc = regAllocObj.regDescriptor
			currIndex = tacString.find(line)
			varToFree = nextUseHueristics(currIndex,regDesc)

			reg3,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
			if asmInstruc != "":outputMain.append(asmInstruc)
			#outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
			#outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
			outputMain.append("div "+reg2+","+reg3) # $t1 = $t1 % $t2
			outputMain.append("mflo "+reg1) # $t1 = low
	elif word[0] == "ifgoto":
		if debug:
			print "ifgoto entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[2].isdigit() and varList.count(word[2]) == 0:
			varList.append(word[2])
			if word[2] not in initList:outputData.append(word[2]+": .space 4")
		if not word[3].isdigit() and varList.count(word[3]) == 0:
			varList.append(word[3])
			if word[3] not in initList:outputData.append(word[3]+": .space 4")

		if word[1] == "==":
			if debug:
				print "ifgoto == entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)
				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				
				
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("beq "+reg1+","+reg2+",Label_"+word[4])
		elif word[1] == ">=":
			if debug:
				print "ifgoto >= entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)

				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bge "+reg1+","+reg2+","+word[4])
		elif word[1] == "<=":
			if debug:
				print "ifgoto <= entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("ble "+reg1+","+reg2+",Label_"+word[4])
		elif word[1] == "<":
			if debug:
				print "ifgoto < entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("blt "+reg1+","+reg2+",Label_"+word[4])
		elif word[1] == ">":
			if debug:
				print "ifgoto > entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bgt "+reg1+","+reg2+",Label_"+word[4])
		elif word[1] == "!=":
			if debug:
				print "ifgoto != entered"
			if not word[2].isdigit() and not word[3].isdigit(): 
				reg1,asmInstruc = regAllocObj.getRegister(word[2],word[2],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				
				regDesc = regAllocObj.regDescriptor
				currIndex = tacString.find(line)
				varToFree = nextUseHueristics(currIndex,regDesc)

				reg2,asmInstruc = regAllocObj.getRegister(word[3],word[3],varToFree)
				if asmInstruc != "":outputMain.append(asmInstruc)
				# outputMain.append("lw $t1,"+word[2]) # $t1 = variable 1
				# outputMain.append("lw $t2,"+word[3]) # $t2 = variable 2
				outputMain.append("bne "+reg1+","+reg2+",Label_"+word[4])

	elif word[0] == "goto":
		if debug:
			print "goto entered"
		outputMain.append("b Label_"+word[1])

	elif word[0] == "print_int":
		if debug:
			print "print_int entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		outputMain.append("li $v0,1")
		if '(' in reg1:
			outputMain.append("lw $a0,"+reg1)
		else:
			outputMain.append("move $a0,"+reg1)
		
		outputMain.append("syscall")

	elif word[0] == "print_char":
		if debug:
			print "print_char entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		if '(' in reg1:
			outputMain.append("lw $a0,"+reg1)
		else:
			outputMain.append("move $a0,"+reg1)
		#outputMain.append("move $a0,"+reg1)
		outputMain.append("li $v0,11")
		outputMain.append("syscall")

	elif word[0] == "print_string":
		if debug:
			print "print_string entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		if '(' in reg1:
			outputMain.append("lw $a0,"+reg1)
		else:
			outputMain.append("move $a0,"+reg1)
		#outputMain.append("move $a0,"+reg1)
		outputMain.append("li $v0,4")
		outputMain.append("syscall")

	elif word[0] == "print_bool":
		if debug:
			print "print_bool entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		if '(' in reg1:
			outputMain.append("lw $a0,"+reg1)
		else:
			outputMain.append("move $a0,"+reg1)
		#outputMain.append("move $a0,"+reg1)
		outputMain.append("li $v0,1")
		outputMain.append("syscall")

	elif word[0] == "read_int":
		if debug:
			print "read_int entered"


		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		
		outputMain.append("li $v0,5")
		outputMain.append("syscall")
		outputMain.append("move "+reg1+",$v0")

	elif word[0] == "read_char":
		if debug:
			print "read_char entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		
		outputMain.append("li $v0,12")
		outputMain.append("syscall")
		outputMain.append("move "+reg1+",$v0")

	elif word[0] == "read_string":
		if debug:
			print "read_string entered"

		regDesc = regAllocObj.regDescriptor
		currIndex = tacString.find(line)
		varToFree = nextUseHueristics(currIndex,regDesc)

		if not word[1].isdigit() and varList.count(word[1]) == 0:
			varList.append(word[1])
			if word[1] not in initList:outputData.append(word[1]+": .space 4")
		reg1,asmInstruc = regAllocObj.getRegister(word[1],word[1],varToFree)
		if asmInstruc != "":outputMain.append(asmInstruc)
		
		outputMain.append("li $v0,8")
		outputMain.append("la $a0,"+word[1])
		outputMain.append("la $a1,"+str(word[2])) #word[2] is length of string buffer passed from tac
		outputMain.append("syscall")
		

outputMain = []
outputData = []
outputText = []
exitFound = False
regDesc = []
varList = []
initList = []
tacString = ""

if __name__ == '__main__':
	with open(sys.argv[1], 'r') as my_file:
		#print(my_file.read())
		inputFile = my_file.read()
	tac = ""
	for i in range(len(inputFile)):
		tac = tac + inputFile[i]
	tacLines = tac.split('\n')
	#print len(tac)

	outputData.append(".data")
	outputText.append(".text")
	outputMain.append("main:")

	for i in range(len(tacLines)-1):
		tacString = tacString + ',' +tacLines[i]
	tacString = tacString + ','
	#print tacString

	for i in range(len(tacLines)-1):
		convertToAsm(tacLines[i],i+1)
		# tacc = tac[i].split(',')
		# for j in range(len(tacc)):
		# 	print tacc[j]
	#remove all initializers like "arr[1] .space 10"
	for i in range(len(outputData)-1):
		if '[' in outputData[i]:
			del outputData[i]
		#print outputData[i]
	if '[' in outputData[-1]:
		del outputData[-1]

	output = outputData + outputText + outputMain
	#obj.callfunc()
	fileobject = open("file.asm","w")
	for i in range(len(output)):
		print output[i]
		fileobject.write(output[i]+"\n")
	fileobject.close()
	#print regAllocObj.regDescriptor
	# print regAllocObj.varDescriptor