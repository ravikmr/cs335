#Data Types:
int (as per assignment)
array
string (only read and print)
char (only read and print)
bool (only read and print)

#Operators:
Arithmetic: +,-,/,*,%
Relational: <,>,<=,>=,==,!=
Bitwise: <<,>>,&,|,^
Logical: &&,||
Assignment: =,+=,-=,*=,/=,%=,^=,&=,|=

#Loops

#Selection statements
if-else

#Jump statements
return

#Functions
Simple as well as nested
Return types: int

