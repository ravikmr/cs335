#Group Members: Ravi Kumar

#steps to run:
cd asgn2
make
bin/codegen test/tac1.txt

#output
This will generate mips code on terminal. Also, one file.asm is created in the asgn2 folder which can be directly run into spim simulator.


#steps to clean
make clean

#test files:
We have used 5 test files to test out mips code.
test/tac1.txt has basic arithmetic operations
test/tac2.txt has nested function implementation
test/tac3.txt has array implementation
test/tac4.txt has loop implementation
test/tac5.txt has relational operators, arithmetic operators, nested functions, array, print statements, unary operators, etc etc.

#Note:
Use python bin/codegen.py test/tac1.txt if bin/codegen test/tac1.txt is not working.
