
namespace RegExApplication
{
   class Program
   {
       void showMatch(char text, char expr)
      {
         console.writeline("The Expression: " + expr);
         MatchCollection mc = Regex.Matches(text, expr);
         foreach (Match m in mc)
         {
            console.writeline(m);
         }
      }
      void Main()
      {
         char str = "make maze and manage to measure it";

         console.writeline("Matching words start with 'm' and ends with 'e':");
         showMatch(str, @"\bm\S*e\b");
         console.readline();
      }
   }
}
