namespace Ackermann
{
	class Ackermann_test
	{
		 void Main()
		{
			int m = 0,n=0;

			console.writeline("Value of Ackermann(m, n)");

			console.writeline("m = ");
			m = console.readline();

			console.writeline("n = ");
			n = console.readline();

			console.writeline();
			console.writeline("Ackermann({0}, {1}) = {2}", m, n, Ackermann(m, n));
			console.readline();
		}
        int Ackermann(int m, int n)
		{
			if (m == 0)
				return n + 1;
			else
				if (n == 0)
					return Ackermann(m - 1, 1);
				else
					return Ackermann(m - 1, Ackermann(m, n - 1));
		}
	}
    
		
	
}
