
namespace IsaacNewto{
 
 class INMath{
 INMath() { }
/// <summary>
/// Newton-Raphson method --Calculate square root.
/// </summary>
/// <param name="n">The square root of n.</param>
/// <returns>Returns the square root of n.</returns>
public  double Sqrt(double n){
 
double inNum1 = n / 2, inNum2;
inNum2 = (inNum1 + (n / inNum1)) / 2;
do
{
inNum1 = inNum2;
inNum2 = (inNum1 +n / inNum1) / 2;
} while (Math.Abs(inNum1 - inNum2) >=Math.Pow(10,-5)); // approximate value.
return inNum1;
}
}
class Program{
 
 void Main(){
 
console.writeline(Math.Sqrt(36.0));
console.writeline(INMath.Sqrt(36.0));
}
}
}
