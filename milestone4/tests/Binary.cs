namespace forgetCode
{
    class Program
    {
        void Main()
        {
            int[] a = new int[100];
            console.writeline("Number of elements in the array ?");
            int s = console.readline();
            int x = Parse(s);
            console.writeline("-----------------------");
            console.writeline(" Enter array elements ");
            console.writeline("-----------------------");
            for (int i = 0; i < x; i++)
            {
                int s1 = console.readline();
                a[i] = Parse(s1);
            }
            console.writeline("--------------------");
            console.writeline("Enter Search element");
            console.writeline("--------------------");
            int s3 = console.readline();
            int x2 = Parse(s3);
            int low = 0;
            int high = x - 1;
            while (low <= high)
            {
                int mid = (low + high) / 2;
                if (x2 < a[mid])
                    high = mid - 1;
                else if (x2 > a[mid])
                    low = mid + 1;
                else if (x2 == a[mid])
                {
                    console.writeline("-----------------");
                    console.writeline("Search successful");
                    console.writeline("-----------------");
                    console.writeline("Element {0} found at location {1}\n", x2, mid + 1);
                    return;
                }
            }
            console.writeline("Search unsuccessful");
        }
    }
}
