 

    namespace matrix_multiplication

    {

        class Program

        {

            void Main()

            {

                int i, j,m,n;

                console.writeline("Enter the Number of Rows and Columns : ");

                m = (console.ReadLine());

                n = (console.ReadLine());

                int[,] a = new int[m, n];

                console.writeline("Enter the First Matrix");

                for (i = 0; i < m; i++)

                {

                    for (j = 0; j < n; j++)

                    {

                        a[i, j] = (console.ReadLine());

                    }

                }

                console.writeline("First matrix is:");

                for (i = 0; i < m; i++)

                {

                    for (j = 0; j < n; j++)

                    {

                        console.Write(a[i, j] + "\t");

                    }

                    console.writeline();

                }

                int[,] b = new int[m, n];

                console.writeline("Enter the Second Matrix");

                for (i = 0; i < m; i++)

                {

                    for (j = 0; j < n; j++)

                    {

                        b[i, j] = (console.ReadLine());

                    }

                }

                console.writeline("Second Matrix is :");

                for (i = 0; i < 2; i++)

                {

                    for (j = 0; j < 2; j++)

                    {

                        console.Write(b[i, j] + "\t");

                    }

                    console.writeline();

                }

                console.writeline("Matrix Multiplication is :");

                int[,] c = new int[m, n];

                for (i = 0; i < m; i++)

                {

                    for (j = 0; j < n; j++)

                    {

                        c[i, j] = 0;

                        for (int k = 0; k < 2; k++)

                        {

                            c[i, j] += a[i, k] * b[k, j];

                        }

                    }

                }

                for (i = 0; i < m; i++)

                {

                    for (j = 0; j < n; j++)

                    {

                        console.Write(c[i, j] + "\t");

                    }

                    console.writeline();

                }

     

                console.ReadKey();

            }

        }

    }
