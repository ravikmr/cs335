import sys
import regAllocator
import ply.yacc as yacc
import lexer
from sys import argv
import symbolTable as SymbolTable
from tac import *
import parser 


# ST = SymbolTable.SymbolTable()
TAC = ThreeAddressCode()

mips = regAllocator.regAllocator()

# reg1,asmInstruc = mips.getRegister("a")
# print reg1
# reg1,asmInstruc = mips.getRegister("b")
# print reg1
# reg1,asmInstruc = mips.getRegister("a")
# print reg1

def printsym():
	ST.returnOffset("t1")



def convertToAsm(line,lineNum):
	global exitFound
	asmInstruc = ""
	word = line
	

	if word[0] == "#":
		#comment
		pass
	elif word[0] == "Main_Level:":
		mips.code.append("main:")
		# ST.printSymbolTable()
		# key_offset=ST.getAttribute('t1', "offset")
		# print ST.getAttribute('t1','type')


	elif word[0] == "=":
		
		
		if word[1].isdigit():
			reg1 = mips.getRegister(word[3])
			mips.code.append("li "+reg1+", "+str(word[1]))
		else:
			reg1 = mips.getRegister(word[1])
			reg2 = mips.getRegister(word[3])
			mips.code.append("move "+reg2+", "+reg1)
	elif word[0] == "PrintInt":
		mips.code.append("li $v0, 1")
		reg1 = mips.getRegister(word[3])
		mips.code.append("move $a0, "+reg1)
		mips.code.append("syscall")
	elif word[0] == "exit":
		mips.code.append("li $v0, 10")
		mips.code.append("syscall")
	elif word[0] == '+':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append("add "+ reg3 + ", " + reg1 +", "+reg2)
	elif word[0] == '-':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append("sub "+ reg3 + ", " + reg1 +", "+reg2)

	elif word[0] == 'goto':
		mips.code.append("b"+" "+ word[3])

	elif word[0] == 'level':
		mips.code.append('level:')

	elif word[0] == 'ifgoto':
		reg1 = mips.getRegister(word[1])
		if word[2] == 0:
			mips.code.append('beqz '+reg1+ ' '+	word[3])
		elif word[2] == 1:
			mips.code.append('bnez '+reg1+ ' '+	word[3])
	elif word[0] == '*':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('mult '+reg1+', '+reg2)
		mips.code.append('mflo '+reg3)
	elif word[0] == '/':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('div '+reg1+', '+reg2)
		mips.code.append('mflo '+reg3)

	elif word[0] == '%':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('div '+reg1+', '+reg2)
		mips.code.append('mfhi '+reg3)

	elif word[0] == '+=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('add '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '|':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('or '+reg3+', '+reg1+', '+reg2)
	
	elif word[0] == '^':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('xor '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '&':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('and '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '==':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('seq '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '!=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('sne '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '<':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('slt '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '>':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('sgt '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '<=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('sle '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '>=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('sge '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '<<':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('sllv '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '>>':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[2])
		reg3 = mips.getRegister(word[3])
		mips.code.append('srlv '+reg3+', '+reg1+', '+reg2)

	elif word[0] == '-=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('sub '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '*=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('mult '+reg2+', '+reg1)
		mips.code.append('mflo '+reg2)

	elif word[0] == '/=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('div '+reg2+', '+reg1)
		mips.code.append('mflo '+reg2)

	elif word[0] == '%=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('div '+reg2+', '+reg1)
		mips.code.append('mfhi '+reg2)

	elif word[0] == '^=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('xor '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '|=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('or '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '&=':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('and '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '<<==':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('sllv '+reg2+', '+reg2+', '+reg1)

	elif word[0] == '>>==':
		reg1 = mips.getRegister(word[1])
		reg2 = mips.getRegister(word[3])
		mips.code.append('srlv '+reg2+', '+reg2+', '+reg1)

	elif word[0] == 'funcLabel':
		mips.code.append(word[1])

	elif word[0] == 'callFunc':
		regDesc = mips.regDescripter
		for key,value in regDesc.iteritems():
			mips.code.append("addi $sp,$sp,-4")
			mips.code.append("sw "+key+",($sp)")
		mips.code.append("addi $sp,$sp,-4")
		mips.code.append("sw $ra,($sp)")
		mips.code.append("jal "+word[3])
		mips.code.append("lw $ra,($sp)")
		mips.code.append("addi $sp,$sp,4")

		for key,value in regDesc.iteritems():

			mips.code.append("lw "+key+", ($sp)")
			mips.code.append("addi $sp,$sp,4")

	elif word[0] == "ret":
		mips.code.append("jr $ra")




outputMain = []
outputData = []
outputText = []
exitFound = False
regDesc = []
varList = []
initList = []
tacString = ""

if __name__ == '__main__':
	input_ = open(argv[1]).read()

	TAC, ST = parser.parseProgram(input_)
	
	outputData.append(".data")
	mips.code.append(".text")
	# printsym()
	# ST.printSymbolTable()
	# ST.printFunctionList()
	# mips.code.append("main:")
	# print str(ST.getAttributeFromFunctionList('main','width'))
	mips.code.append('sub $sp, $sp, '+str(ST.getAttributeFromFunctionList('main','width')))
	mips.code.append('jal '+ 'main')
	mips.code.append('add $sp, $sp, '+str(ST.getAttributeFromFunctionList('main','width')))
	mips
	lineNum = 0
	for i in ST.string:
		#print ST.string[i]
		outputData.append(i+':'+" .asciiz " + ST.string[i]) 
	for line in TAC.code:
		lineNum = lineNum +1
		#print line
		convertToAsm(line,lineNum)

	for i in outputData + mips.code:
		print i
		

