using System;
 
namespace Example3
{
  class Program
   {
     static void Main(string[] args)
      {
        int num1,num2,result;
        num1 = Convert.ToInt32(args[0]);
        num2 = Convert.ToInt32(args[1]);
        result = num1 * num2;
        Console.WriteLine("{0} x {1} = {2}", num1, num2,            result);
      }
   }
}
