import re
import os
import sys
import ply.lex as lex
import ply.yacc as yacc
import lexer
from sys import argv
import subprocess
tokens = lexer.tokens
lexer = lexer.lexer
precedence = (
    ('left', 'ADDITION', 'SUBTRACTION', 'CLOSEPARENTHESIS',),
    ('left', 'MULTIPLICATION', 'DIVISION'),
    ('right','ELSE')
)
inword = "'"
outword = "'"
indicator = "   &nbsp;&nbsp;&nbsp;&nbsp;==>>&nbsp;&nbsp;&nbsp;&nbsp;   "
def toHTML(p):
	a=p.slice
	stringToAdd = "<b>"+str(a[0]) + "</b> &nbsp;&nbsp;&nbsp;&nbsp;==>>&nbsp;&nbsp;&nbsp;&nbsp; "
	for i in range(1,len(a)):
		if isinstance(a[i],lex.LexToken):
			stringToAdd = stringToAdd + " <font color=\"red\">" + a[i].value + "</font>"
		else:
			stringToAdd = stringToAdd + " " +str(a[i])
	stringToAdd = stringToAdd + "\n\n<br><br>"
	fo.write(stringToAdd)
def special_match(strg, search=re.compile(r'[^a-z-]').search):
	return not bool(search(strg))
def p_compilation_unit(p):
	''' compilation_unit : namespace-member-declaration'''
	toHTML(p)
	
def p_namespace_member_declaration(p):
	''' namespace-member-declaration : NAMESPACE IDENTIFIER namespace-body'''
	toHTML(p)
	
def p_namespace_body(p):
	''' namespace-body : BLOCKBEGIN class-declarations BLOCKEND '''
	toHTML(p)
	
def p_class_declarations(p):
    ''' class-declarations : class-declaration
             			   | class-declarations class-declaration'''
    toHTML(p)
    
def p_class_declaration(p):
	''' class-declaration : CLASS IDENTIFIER class-body'''
	toHTML(p)


def p_class_type(p):
    ''' class-type :         IDENTIFIER
             '''
    toHTML(p)

def p_class_modifier(p):
	''' modifier : PUBLIC
				 | PROTECTED
				 | PRIVATE
				 | NEW
				 | ABSTRACT '''
	toHTML(p)

def p_class_body(p):
	''' class-body : BLOCKBEGIN class-member-declarations BLOCKEND'''
	toHTML(p)

def p_class_member_declarations(p):
	''' class-member-declarations : class-member-declaration
								  | class-member-declaration class-member-declarations'''
	toHTML(p)
								
def p_class_member_declaration(p):
	'''class-member-declaration : constant-declaration
								| field-declaration
								| method-declaration
								| constructor-declaration
								| destructor-declaration
								'''
	toHTML(p)	

def p_constant_declaration(p):
    ''' constant-declaration : modifier CONST type constant-declarators SEMICOLON
             				|  CONST type constant-declarators SEMICOLON
             '''
    toHTML(p)
	
def p_constant_declarators(p):
    ''' constant-declarators : constant-declarator
             				 | constant-declarators COMMA constant-declarator
             '''
    toHTML(p)
    
def p_constant_declarator(p):
    ''' constant-declarator : IDENTIFIER ASSIGN constant-expression
             '''
    toHTML(p)

def p_constant_expression(p):
	''' constant-expression : expression '''
	toHTML(p)

def p_expression(p):
    ''' expression : conditional-expression
             	   | assignment
             '''
    toHTML(p)
    
def p_conditional_expression(p):
    ''' conditional-expression : conditional-or-expression
             				   | conditional-or-expression CONDITIONALOPERATOR expression COLON expression
             '''
    toHTML(p)
    
def p_conditional_or_expression(p):
    ''' conditional-or-expression : conditional-and-expression
            					  | conditional-or-expression LOGICALOR conditional-and-expression
             '''
    toHTML(p)
    
def p_conditional_and_expression(p):
    ''' conditional-and-expression : inclusive-or-expression
             					   | conditional-and-expression LOGICALAND inclusive-or-expression
             '''
    toHTML(p)
    
def p_inclusive_or_expression(p):
    ''' inclusive-or-expression : exclusive-or-expression
             					| inclusive-or-expression BITOR exclusive-or-expression
             '''
    toHTML(p)
    
def p_exclusive_or_expression(p):
    ''' exclusive-or-expression : and-expression
             					| exclusive-or-expression BITXOR and-expression
             '''
    toHTML(p)
    
def p_and_expression(p):
    ''' and-expression : equality-expression
             		   | and-expression BITAND equality-expression
             '''
    toHTML(p)
    
def p_equality_expression(p):
    ''' equality-expression : relational-expression
             				| equality-expression EQUALITY relational-expression
             				| equality-expression NOTEQUAL relational-expression
             '''
    toHTML(p)
    
def p_relational_expression(p):
    ''' relational-expression : shift-expression
             				  | relational-expression LESSTHAN shift-expression
             				  | relational-expression GREATERTHAN shift-expression
					          | relational-expression LESSTHANEQUAL shift-expression
					          | relational-expression GREATERTHANEQUAL shift-expression
					          '''
    toHTML(p)
    
def p_shift_expression(p):
    ''' shift-expression : additive-expression
             			 | shift-expression LEFTSHIFT additive-expression
             			 | shift-expression RIGHTSHIFT additive-expression
             			 '''
    toHTML(p)
    
def p_additive_expression(p):
    ''' additive-expression : multiplicative-expression
             				| additive-expression ADDITION multiplicative-expression
             				| additive-expression SUBTRACTION multiplicative-expression
             				'''
    toHTML(p)
    
def p_multiplicative_expression(p):
    ''' multiplicative-expression : unary-expression
             					  | multiplicative-expression MULTIPLICATION unary-expression
                                  | multiplicative-expression DIVISION unary-expression
             					  | multiplicative-expression MODULUS unary-expression
             					  '''
    toHTML(p)
    
def p_unary_expression(p):
    ''' unary-expression :         primary-expression
             |         ADDITION unary-expression
             |         SUBTRACTION unary-expression
             |         BITNOT unary-expression
             |         BITCOMPLEMENT unary-expression
             |         MULTIPLICATION unary-expression
             |         pre-increment-expression
             |         pre-decrement-expression
             '''
    toHTML(p)
    
def p_primary_expression(p):
    ''' primary-expression : array-creation-expression
             |         primary-no-array-creation-expression
             '''
    toHTML(p)

def p_array_creation_expression(p):
    ''' array-creation-expression : NEW simple-type OPENSQUAREBRACKET expression-list CLOSESQUAREBRACKET array-initializer-opt
             '''
    toHTML(p)

def p_array_initializer_opt(p):
    ''' array-initializer-opt :         array-initializer
             |         empty
             '''
    toHTML(p)

def p_expression_list(p):
    ''' expression-list :         expression
             |         expression-list COMMA expression
             '''
    toHTML(p)
    
def p_array_initializer(p):
    ''' array-initializer :         BLOCKBEGIN variable-initializer-list-opt BLOCKEND
             '''
    toHTML(p)

def p_variable_initializer_list_opt(p):
    ''' variable-initializer-list-opt :         variable-initializer-list
             |         empty
             '''
    toHTML(p)
    
def p_variable_initializer_list(p):
    ''' variable-initializer-list :         variable-initializer
             |         variable-initializer-list COMMA variable-initializer
             '''
    toHTML(p)
    
def p_variable_initializer(p):
    ''' variable-initializer :         expression
             |         array-initializer
             '''
    toHTML(p)

def p_primary_no_array_creation_expression(p):
    ''' primary-no-array-creation-expression :         literal
             |         IDENTIFIER
             |         parenthesized-expression
             |         member-access
             |         invocation-expression
             |         element-access
             |         this-access
             |         base-access
             |         post-increment-expression
             |         post-decrement-expression
             |         object-creation-expression
             '''
    toHTML(p)

def p_parenthesized_expression(p):
    ''' parenthesized-expression :         OPENPARENTHESIS expression CLOSEPARENTHESIS
             '''
    toHTML(p)

def p_member_access(p):
    ''' member-access :         primary-expression DOT IDENTIFIER
             '''
    toHTML(p)

def p_invocation_expression(p):
    ''' invocation-expression :         primary-expression OPENPARENTHESIS argument-list-opt CLOSEPARENTHESIS
             '''
    toHTML(p)

def p_argument_list_opt(p):
    ''' argument-list-opt :         argument-list
             |         empty
             '''
    toHTML(p)
    
def p_argument_list(p):
    ''' argument-list :         argument
             |         argument-list COMMA argument
             '''
    toHTML(p)
    
def p_argument(p):
    ''' argument :         expression
             |         OUT variable-reference
             '''
    toHTML(p)

def p_variable_reference(p):
    ''' variable-reference :         expression
             '''
    toHTML(p)

def p_element_access(p):
    ''' element-access :         primary-no-array-creation-expression OPENSQUAREBRACKET expression-list CLOSESQUAREBRACKET
             '''
    toHTML(p)

def p_this_access(p):
    ''' this-access :         THIS
             '''
    toHTML(p)

def p_base_access(p):
    ''' base-access :         BASE DOT IDENTIFIER
             |         BASE OPENSQUAREBRACKET expression-list CLOSESQUAREBRACKET
             '''
    toHTML(p)
    
def p_post_increment_expression(p):
    ''' post-increment-expression :         primary-expression INCREMENT
             '''
    toHTML(p)

def p_post_decrement_expression(p):
    ''' post-decrement-expression :         primary-expression DECREMENT
             '''
    toHTML(p)

def p_object_creation_expression(p):
    ''' object-creation-expression :         NEW type OPENPARENTHESIS argument-list-opt CLOSEPARENTHESIS
             '''
    toHTML(p)

def p_pre_increment_expression(p):
    ''' pre-increment-expression :         INCREMENT unary-expression
             '''
    toHTML(p)

def p_pre_decrement_expression(p):
    ''' pre-decrement-expression :         DECREMENT unary-expression
             '''
    toHTML(p)

def p_assignment(p):
    ''' assignment :         primary-expression assignment-operator expression
             '''
    toHTML(p)

def p_assignment_operator(p):
    ''' assignment-operator :         ASSIGN
             |         PLUSEQUAL
             |         MINUSEQUAL
             |         MULTIPLYEQUAL
             |         DIVIDEEQUAL
             |         MODULUSEQUAL
             |         ANDEQUAL
             |         OREQUAL
             |         XOREQUAL
             |         LEFTSHIFTEQUAL
             |         RIGHTSHIFTEQUAL
             '''
    toHTML(p)
    
def p_field_declaration(p):
    ''' field-declaration :         modifier type variable-declarators SEMICOLON
             |         type variable-declarators SEMICOLON
             '''
    toHTML(p)
    
def p_variable_declarators(p):
    ''' variable-declarators :         variable-declarator
             |         variable-declarators COMMA variable-declarator
             '''
    toHTML(p)

def p_variable_declarator(p):
    ''' variable-declarator :         IDENTIFIER
             |         IDENTIFIER ASSIGN variable-initializer
             '''
    toHTML(p)

def p_method_declaration(p):
    ''' method-declaration :         method-header method-body
             '''
    toHTML(p)

def p_method_header(p):
    ''' method-header :         modifier type member-name OPENPARENTHESIS formal-parameter-list-opt CLOSEPARENTHESIS
             |         modifier VOID member-name OPENPARENTHESIS formal-parameter-list-opt CLOSEPARENTHESIS
             |         type member-name OPENPARENTHESIS formal-parameter-list-opt CLOSEPARENTHESIS
             |         VOID member-name OPENPARENTHESIS formal-parameter-list-opt CLOSEPARENTHESIS
             '''
    toHTML(p)

def p_formal_parameter_list_opt(p):
    ''' formal-parameter-list-opt :         formal-parameter-list
             |         empty
             '''
    toHTML(p)
    
def p_member_name(p):
    ''' member-name :         IDENTIFIER
             '''
    toHTML(p)

def p_formal_parameter_list(p):
    ''' formal-parameter-list :         fixed-parameters
             '''
    toHTML(p)

def p_fixed_parameters(p):
    ''' fixed-parameters :         fixed-parameter
             |         fixed-parameters COMMA fixed-parameter
             '''
    toHTML(p)
def p_fixed_parameter(p):
    ''' fixed-parameter :         parameter-modifier-opt type IDENTIFIER
             '''
    toHTML(p)
def p_parameter_modifier_opt(p):
    ''' parameter-modifier-opt :         parameter-modifier
             |         empty
             '''
    toHTML(p)
def p_parameter_modifier(p):
    ''' parameter-modifier :         OUT
             '''
    toHTML(p)
def p_method_body(p):
    ''' method-body : block
    				| SEMICOLON
             '''
    toHTML(p)
def p_block(p):
    ''' block :         BLOCKBEGIN statement-list-opt BLOCKEND
             '''
    toHTML(p)
def p_statement_list_opt(p):
    ''' statement-list-opt :         statement-list
             |         empty
             '''
    toHTML(p)
def p_statement_list(p):
    ''' statement-list :         statement
             |         statement-list statement
             '''
    toHTML(p)
def p_statement(p):
    ''' statement :         labeled-statement
             |         declaration-statement
             |         embedded-statement
             '''
    toHTML(p)
def p_labeled_statement(p):
    ''' labeled-statement :         IDENTIFIER COLON statement
             '''
    toHTML(p)
def p_declaration_statement(p):
    ''' declaration-statement :         local-variable-declaration SEMICOLON
             |         local-constant-declaration SEMICOLON empty
             '''
    toHTML(p)
def p_local_variable_declaration(p):
    ''' local-variable-declaration :         type local-variable-declarators
             '''
    toHTML(p)
def p_local_variable_declarators(p):
    ''' local-variable-declarators :         local-variable-declarator
             |         local-variable-declarators COMMA local-variable-declarator
             '''
    toHTML(p)
def p_local_variable_declarator(p):
    ''' local-variable-declarator :         IDENTIFIER
             |         IDENTIFIER ASSIGN local-variable-initializer
             '''
    toHTML(p)
def p_local_variable_initializer(p):
    ''' local-variable-initializer :         expression
             |         array-initializer
             '''
    toHTML(p)
def p_local_constant_declaration(p):
    ''' local-constant-declaration :         CONST type constant-declarators
             '''
    toHTML(p)
def p_embedded_statement(p):
    ''' embedded-statement :         block
             |         empty-statement
             |         expression-statement
             |         selection-statement
             |         iteration-statement
             |         jump-statement
             '''
    toHTML(p)
def p_empty_statement(p):
    ''' empty-statement :         SEMICOLON
             '''
    toHTML(p)
def p_expression_statement(p):
    ''' expression-statement :         statement-expression SEMICOLON
             '''
    toHTML(p)
def p_statement_expression(p):
    ''' statement-expression :         invocation-expression
             |         object-creation-expression
             |         assignment
             |         post-increment-expression
             |         post-decrement-expression
             |         pre-increment-expression
             |         pre-decrement-expression
             '''
    toHTML(p)
def p_selection_statement(p):
    ''' selection-statement :         if-statement
             |         switch-statement
             '''
    toHTML(p)
def p_if_statement(p):
    ''' if-statement :         IF OPENPARENTHESIS boolean-expression CLOSEPARENTHESIS embedded-statement
             |         IF OPENPARENTHESIS boolean-expression CLOSEPARENTHESIS embedded-statement ELSE embedded-statement
             '''
    toHTML(p)
def p_boolean_expression(p):
    ''' boolean-expression :         expression
             '''
    toHTML(p)
def p_switch_statement(p):
    ''' switch-statement :         SWITCH OPENPARENTHESIS expression CLOSEPARENTHESIS switch-block
             '''
    toHTML(p)
def p_switch_block(p):
    ''' switch-block :         BLOCKBEGIN switch-sections-opt BLOCKEND
             '''
    toHTML(p)
def p_switch_sections_opt(p):
    ''' switch-sections-opt :         switch-sections
             |         empty
             '''
    toHTML(p)
def p_switch_sections(p):
    ''' switch-sections :         switch-section
             |         switch-sections switch-section
             '''
    toHTML(p)
def p_switch_section(p):
    ''' switch-section :         switch-labels statement-list
             '''
    toHTML(p)
def p_switch_labels(p):
    ''' switch-labels :         switch-label
             |         switch-labels switch-label
             '''
    toHTML(p)
def p_switch_label(p):
    ''' switch-label :         CASE expression COLON
             |         DEFAULT COLON
             '''
    toHTML(p)
def p_iteration_statement(p):
    ''' iteration-statement :         while-statement
             |         for-statement
             |         foreach-statement
             |         do-statement
             '''
    toHTML(p)
def p_while_statement(p):
    ''' while-statement : WHILE OPENPARENTHESIS boolean-expression CLOSEPARENTHESIS embedded-statement
             '''
    toHTML(p)
def p_do_statement(p):
    ''' do-statement : DO embedded-statement WHILE OPENPARENTHESIS boolean-expression CLOSEPARENTHESIS SEMICOLON
             '''
    toHTML(p)
def p_do_statement_error(p):
    ''' do-statement : DO embedded-statement WHILE OPENPARENTHESIS boolean-expression CLOSEPARENTHESIS error
             '''
    print 'Semicolon is missing in do-while loop in lineno ' + str(p.lineno(1))

def p_for_statement(p):
    ''' for-statement : FOR OPENPARENTHESIS for-initializer-opt SEMICOLON for-condition-opt SEMICOLON for-iterator-opt CLOSEPARENTHESIS embedded-statement
             '''
    toHTML(p)
def p_for_statement_error1(p):
    ''' for-statement : FOR OPENPARENTHESIS for-initializer-opt error for-condition-opt SEMICOLON for-iterator-opt CLOSEPARENTHESIS embedded-statement
             '''
    print 'first Semicolon missing in for-loop lineno'+ str(p.lineno(1))
def p_for_statement_error2(p):
    ''' for-statement : FOR OPENPARENTHESIS for-initializer-opt SEMICOLON for-condition-opt error for-iterator-opt CLOSEPARENTHESIS embedded-statement
             '''
    print 'Second Semicolon missing in for-loop lineno'+ str(p.lineno(1))


def p_for_initializer_opt(p):
    ''' for-initializer-opt :         for-initializer
             |         empty
             '''
    toHTML(p)
def p_for_initializer(p):
    ''' for-initializer :         local-variable-declaration
             |         statement-expression-list
             '''
    toHTML(p)
def p_for_condition_opt(p):
    ''' for-condition-opt :         for-condition
             |         empty
             '''
    toHTML(p)
def p_for_condition(p):
    ''' for-condition :         boolean-expression
             '''
    toHTML(p)
def p_for_iterator_opt(p):
    ''' for-iterator-opt :         for-iterator
             |         empty
             '''
    toHTML(p)
def p_for_iterator(p):
    ''' for-iterator :         statement-expression-list
             '''
    toHTML(p)
def p_statement_expression_list(p):
    ''' statement-expression-list :         statement-expression
             |         statement-expression-list COMMA statement-expression
             '''
    toHTML(p)
def p_foreach_statement(p):
    ''' foreach-statement : FOREACH OPENPARENTHESIS type IDENTIFIER IN expression CLOSEPARENTHESIS embedded-statement
             '''
    toHTML(p)
def p_jump_statement(p):
    ''' jump-statement :         break-statement
             |         continue-statement
             |         goto-statement
             |         return-statement
             '''
    toHTML(p)
def p_break_statement(p):
    ''' break-statement :         BREAK SEMICOLON
             '''
    toHTML(p)

def p_continue_statement(p):
    ''' continue-statement :         CONTINUE SEMICOLON
             '''
    toHTML(p)
def p_goto_statement(p):
    ''' goto-statement :         GOTO IDENTIFIER SEMICOLON
             '''
    toHTML(p)
def p_goto_statement_error(p):
    ''' goto-statement :         GOTO IDENTIFIER error
             '''
    print 'semicolon missing in goto stat'
def p_return_statement(p):
    ''' return-statement :         RETURN expression-opt SEMICOLON
             '''
    toHTML(p)
def p_expression_opt(p):
    ''' expression-opt :         expression
             |         empty
             '''
    toHTML(p)
def p_constructor_declaration(p):
    ''' constructor-declaration :         constructor-declarator constructor-body
             '''
    toHTML(p)
def p_constructor_declarator(p):
    ''' constructor-declarator :         IDENTIFIER OPENPARENTHESIS formal-parameter-list-opt CLOSEPARENTHESIS 
             '''
    toHTML(p)
def p_constructor_body(p):
    ''' constructor-body :         block
             |         SEMICOLON
             '''
    toHTML(p)
def p_destructor_declaration(p):
    ''' destructor-declaration :  BITCOMPLEMENT IDENTIFIER OPENPARENTHESIS CLOSEPARENTHESIS destructor-body
             '''
    toHTML(p)
def p_destructor_body(p):
    ''' destructor-body :         block
             |         SEMICOLON
             '''
    toHTML(p)
def p_literal(p):
    ''' literal :     SIGNEDINTEGER
             |     UNSIGNEDINTEGER
             |     LONGINTEGER
             |     UNSIGNEDLONGINTEGER
             |     FLOATREAL
             |     DOUBLEREAL
             |     DECIMALREAL
             |     CHARACTER
             |     REGULARSTRING
             |     VERBATIMSTRING
             |     TRUE
             |     FALSE             '''
    toHTML(p)
def p_empty(p):
    'empty :'
    pass
def p_type(p):
    ''' type :         simple-type
             |         class-type
             |         array-type
             '''
    toHTML(p)
def p_simple_type(p):
    ''' simple-type :         numeric-type
             |         BOOL
             '''
    toHTML(p)
def p_numeric_type(p):
    ''' numeric-type :         integral-type
             |         floating-point-type
             '''
    toHTML(p)
def p_integral_type(p):
    ''' integral-type :         INT
             |         UINT
             |         LONG
             |         ULONG
             |         CHAR
             '''
    toHTML(p)
def p_floating_point_type(p):
    ''' floating-point-type :         FLOAT
             |         DOUBLE
             '''
    toHTML(p)
def p_array_type(p):
    ''' array-type :         simple-type rank-specifier
             '''
    toHTML(p)
def p_rank_specifier(p):
    ''' rank-specifier :         OPENSQUAREBRACKET dim-separators-opt CLOSESQUAREBRACKET
             '''
    toHTML(p)
def p_dim_separators_opt(p):
    ''' dim-separators-opt :         dim-separators
             |         empty
             '''
    toHTML(p)
def p_dim_separators(p):
    ''' dim-separators :         COMMA
             |         dim-separators COMMA
             '''
    toHTML(p)
def p_error(p):
    if p:
        print "Syntax error at line " + str(p.lineno)
        print 'Token : {}'.format(p)
    else:
        print("Syntax error!")
    
    #a = 1
    parser.restart()
 #    while 1:
	# tok = yacc.token()
	# if tok:
	# 	if tok.type in ['SEMICOLON','BLOCKEND']:
	# 	    a=0
	# 	    break
 #      #  a=a+1             # Get the next token
 #    yacc.errok()
 #    # tok = yacc.token()
 #    yacc.restart()
 #    return tok


parser = yacc.yacc()

if __name__ == '__main__':
    if os.path.exists("test.html"):
        os.remove("test.html")
    
    fo = open("test.html", "wb")
    input_ = open(argv[1]).read()
    output = parser.parse(input_,lexer=lexer, debug=False, tracking=True)
    fo.close()
    command = "tac test.html > test1.html"
    subprocess.call(command,shell=True)
    command = "rm test.html"
    subprocess.call(command,shell=True)
    #subprocess.call("firefox test1.html",shell=True)    
