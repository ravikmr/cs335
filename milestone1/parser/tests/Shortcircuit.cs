namespace ravi
{
bool Condition1() { return false; }   
bool Condition2() { return true; }     
public A() {       
   if (Condition1() & Condition2())       
   {           
    //inside code will not execute     
   }      
   if (Condition2() | Condition1())    
   {           
   //inside code will execute      
   }   
}   
}

