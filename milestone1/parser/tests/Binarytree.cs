namespace BT
{
    class Node
    {
         int item;
         Node left;
         Node right;
         void display()
        {
            console.writeline("[");
            console.writeline(item);
            console.writeline("]");
        }
    }
    class Tree
    {
         Node root;
         Tree()
        {
            root = 0;
        }
         Node ReturnRoot()
        {
            return root;
        }
         void Insert(int id)
        {
            Node newNode = new Node();
            newNode.item = id;
            if (root == 0)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (id < current.item)
                    {
                        current = current.left;
                        if (current == 0)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == 0)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }
         void Preorder(Node Root)
        {
            if (Root != 0)
            {
                console.writeline(Root.item + " ");
                Preorder(Root.left);
                Preorder(Root.right);
            }
        }
         void Inorder(Node Root)
        {
            if (Root != 0)
            {
                Inorder(Root.left);
                console.writeline(Root.item + " ");
                Inorder(Root.right);
            }
        }
         void Postorder(Node Root)
        {
            if (Root != 0)
            {
                Postorder(Root.left);
                Postorder(Root.right);
                console.writeline(Root.item + " ");
            }
        }
    }
    class Program
    {
         void Main()
        {
            Tree BST = new Tree();
            BST.Insert(30);
            BST.Insert(35);
            BST.Insert(57);
            BST.Insert(15);
            BST.Insert(63);
            BST.Insert(49);
            BST.Insert(89);
            BST.Insert(77);
            BST.Insert(67);
            BST.Insert(98);
            BST.Insert(91);
            console.writeline("Inorder Traversal : ");
            BST.Inorder(BST.ReturnRoot());
            console.writeline(" ");
            console.writeline();
            console.writeline("Preorder Traversal : ");
            BST.Preorder(BST.ReturnRoot());
            console.writeline(" ");
            console.writeline();
            console.writeline("Postorder Traversal : ");
            BST.Postorder(BST.ReturnRoot());
            console.writeline(" ");
            console.readline();
        }
    }

    
}
 
