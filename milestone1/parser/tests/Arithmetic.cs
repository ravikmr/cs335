
namespace Program
{
    class Program
    {
        void Main()
        {
            int Num1, Num2, result;
            char option;
            console.writeline("Enter the First Number : ");
            Num1 = console.readline();
            console.Writeline("Enter the Second Number : ");
            Num2 = console.readline();
            console.writeline("Main Menu");
            console.writeline("1. Addition");
            console.writeline("2. Subtraction");
            console.writeline("3. Multiplication");
            console.writeline("4. Division");
            console.Write("Enter the Operation you want to perform : ");
            option = console.readline();
            switch (option)
            {
            case '1':
                result = Num1 + Num2;
                console.writeline("The result of Addition is : {0}", result);
                break;
            case '2':
                result = Num1 - Num2;
                console.writeline("The result of Subtraction is : {0}", result);
                break;
            case '3':
                result = Num1 * Num2;
                console.writeline("The result of Multiplication is : {0}", result);
                break;
            case '4':
                result = Num1 / Num2;
                console.writeline("The result of Division is : {0}", result);
                break;
            default:
                console.writeline("Invalid Option");
                break;
            }
            console.readline();
        }
 
    }
}
