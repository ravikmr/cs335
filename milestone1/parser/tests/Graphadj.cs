
namespace TheoryOfProgramming
{
    class AdjacencyList
    {
        LinkedList<Tuple<int, int>>[] adjacencyList;
 
        // Constructor - creates an empty Adjacency List
         AdjacencyList(int vertices)
        {
            adjacencyList = new LinkedList<Tuple<int, int>>[vertices];
 
            for (int i = 0; i < adjacencyList.Length; ++i)
            {
                adjacencyList[i] = new LinkedList<Tuple<int, int>>();
            }
        }
 
        // Appends a new Edge to the linked list
         void addEdgeAtEnd(int startVertex, int endVertex, int weight)
        {
            adjacencyList[startVertex].AddLast(new Tuple<int, int>(endVertex, weight));
        }
 
        // Adds a new Edge to the linked list from the front
        public void addEdgeAtBegin(int startVertex, int endVertex, int weight)
        {
            adjacencyList[startVertex].AddFirst(new Tuple<int, int>(endVertex, weight));
        }
 
        // Returns number of vertices
        // Does not change for an object
         int getNumberOfVertices()
        {
            return adjacencyList.Length;
        }
 
        // Returns a copy of the Linked List of outward edges from a vertex
         LinkedList<Tuple<int, int>> this[int index]
        {
            get
            {
                LinkedList<Tuple<int, int>> edgeList
                               = new LinkedList<Tuple<int, int>>(adjacencyList[index]);
 
                return edgeList;
            }
        }
 
        // Prints the Adjacency List
        public void printAdjacencyList()
        {
            int i = 0;
 
            foreach (LinkedList<Tuple<int, int>> list in adjacencyList)
            {
                console.write("adjacencyList[" + i + "] -> ");
 
                foreach (Tuple<int, int> edge in list)
                {
                    console.write(edge.Item1 + "(" + edge.Item2 + ")");
                }
 
                ++i;
                console.writeline();
            }
        }
 
        // Removes the first occurence of an edge and returns true
        // if there was any change in the collection, else false
        public bool removeEdge(int startVertex, int endVertex, int weight)
        {
            Tuple<int, int> edge = new Tuple<int, int>(endVertex, weight);
 
            return adjacencyList[startVertex].Remove(edge);
        }
    }
 
    class TestGraph
    {
        void Main()
        {
            console.writeline("Enter the number of vertices -");
            int vertices = Parse(console.readline());
 
            AdjacencyList adjacencyList = new AdjacencyList(vertices + 1);
 
            console.writeline("Enter the number of edges -");
            int edges = Parse(console.readline());
 
            console.writeline("Enter the edges with weights -");
            int startVertex, endVertex, weight;
 
            for (int i = 0; i < edges; ++i)
            {
                startVertex = Parse(console.readline());
                endVertex = Parse(console.readline());
                weight = Parse(console.readline());
 
                adjacencyList.addEdgeAtEnd(startVertex, endVertex, weight);
            }
 
            adjacencyList.printAdjacencyList();
            adjacencyList.removeEdge(1, 2, 1);
            adjacencyList.printAdjacencyList();
        }
    }
}
